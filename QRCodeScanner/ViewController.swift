//
//  ViewController.swift
//  QRCodeScanner
//
//  Created by 季紅 on 2023/5/24.
//

import UIKit

class ViewController: UIViewController {
    private var qrCodeScanner: QRCodeScannerProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        configurationScanner()
    }

    func configurationScanner() {
        qrCodeScanner = QRCodeScanner()
        qrCodeScanner?.delegate = self

        qrCodeScanner?.startScan(superView: self.view)
    }

}

extension ViewController: QRCodeScannerDelegate {
    func didFailToScanQRCode(error: QRCodeScannerError) {
        switch error {
        case .failToGetCamera:
            print("無法獲取相機裝置")
        case .denyCameraPermission:
            print("尚未開啟相機權限")
        }
    }
    
    func didScanQRCode(value: String) {
        print(value)
    }
}
