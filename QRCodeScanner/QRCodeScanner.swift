//
//  QRCodeScanner.swift
//  QRCodeScanner
//
//  Created by 季紅 on 2023/5/24.
//

import AVFoundation
import UIKit

protocol QRCodeScannerDelegate: AnyObject {
    func didScanQRCode(value: String)
    func didFailToScanQRCode(error: QRCodeScannerError)
}

enum QRCodeScannerError {
    case failToGetCamera
    case denyCameraPermission
}

protocol QRCodeScannerProtocol {
    var delegate: QRCodeScannerDelegate? { get set }
    func startScan(superView: UIView)
    func stopScan()
}

class QRCodeScanner: NSObject, QRCodeScannerProtocol {
    weak var delegate: QRCodeScannerDelegate?
    
    private var captureSession: AVCaptureSession?
    
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    func startScan(superView: UIView) {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(
            deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera, .builtInTrueDepthCamera],
            mediaType: .video,
            position: .back)
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            delegate?.didFailToScanQRCode(error: .failToGetCamera)
            return
        }
        
        guard let input = try? AVCaptureDeviceInput(device: captureDevice) else {
            delegate?.didFailToScanQRCode(error: .denyCameraPermission)
            return
        }
        let captureMetadataOutput = AVCaptureMetadataOutput()

        captureSession = AVCaptureSession()
        captureSession?.addInput(input)
        captureSession?.addOutput(captureMetadataOutput)
     
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: .main)
        captureMetadataOutput.metadataObjectTypes = [.qr]
       
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        videoPreviewLayer?.frame = superView.layer.bounds
        superView.layer.addSublayer(videoPreviewLayer!)

        captureSession?.startRunning()
    }
    
    func stopScan() {
        captureSession?.stopRunning()
        videoPreviewLayer?.removeFromSuperlayer()
    }
    
}

extension QRCodeScanner: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let metadataObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
           metadataObject.type == .qr {
            guard let value = metadataObject.stringValue else {
                return
            }
            delegate?.didScanQRCode(value: value)
        }
    }
}

